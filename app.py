import getpass,os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from flask import Flask, render_template, request, redirect, url_for, session, flash, jsonify
import pymysql.cursors
from flask_cors import CORS, cross_origin
import json
from email.mime.base import MIMEBase
import smtplib

app = Flask(__name__)
CORS(app)
app.secret_key = "sk"

@app.route('/', methods = ['POST'])
def registrando():
    if request.method == 'POST':
        ID = request.form['perid']
        NOMBRE = request.form['pernombre']
        APELLIDO = request.form['perapellido']
        CEDULA = request.form['percedula']
        TELEFONO FIJO = request.form['pertelefonofijo']
        FECHA DE NACIMIENTO = request.form['perfechanacimiento']
       
        conn = pymysql.connect(
        host="localhost", port=3306, user="root",
        passwd="", db="GESTION-LINEAS")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO regis-per VALUES (%s,%s,%s,%s,%s,%s)",(ID,NOMBRE,APELLIDO,CEDULA,TELEFONO FIJO,FECHA DE NACIMIENTO))
        conn.commit()
        conn.close()
        return redirect(url_for('regis-per'))