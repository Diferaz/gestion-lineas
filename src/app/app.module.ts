import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BodyComponent } from './component/body/body.component';
import { EncabezadoComponent } from './component/encabezado/encabezado.component';
import { FooterComponent } from './component/footer/footer.component';
import { RegisLineaComponent } from './component/regis-linea/regis-linea.component';
import { RegisPerComponent } from './component/regis-per/regis-per.component';
import { ContactoComponent } from './component/contacto/contacto.component';
import { RegisEquipoComponent } from './component/regis-equipo/regis-equipo.component';
import { FacturaComponent } from './component/factura/factura.component';

const routes: Routes = [
  { path: 'contacto', component: ContactoComponent },
  { path: 'regis-linea', component: RegisLineaComponent },
  { path: 'regis-equipo', component: RegisEquipoComponent },
  { path: 'factura', component: FacturaComponent },
  { path: '', component: RegisPerComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    EncabezadoComponent,
    FooterComponent,
    RegisLineaComponent,
    RegisPerComponent,
    ContactoComponent,
    RegisEquipoComponent,
    FacturaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
