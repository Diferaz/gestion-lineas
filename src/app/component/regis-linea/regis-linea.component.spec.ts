import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisLineaComponent } from './regis-linea.component';

describe('RegisLineaComponent', () => {
  let component: RegisLineaComponent;
  let fixture: ComponentFixture<RegisLineaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisLineaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisLineaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
