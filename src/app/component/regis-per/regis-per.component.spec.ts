import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisPerComponent } from './regis-per.component';

describe('RegisPerComponent', () => {
  let component: RegisPerComponent;
  let fixture: ComponentFixture<RegisPerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisPerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisPerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
