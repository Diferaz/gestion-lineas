import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisEquipoComponent } from './regis-equipo.component';

describe('RegisEquipoComponent', () => {
  let component: RegisEquipoComponent;
  let fixture: ComponentFixture<RegisEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisEquipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
